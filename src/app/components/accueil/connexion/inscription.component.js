"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var user_1 = require("./../../../Entity/user");
var formValidator_1 = require("../../../common/formValidator");
var InscriptionFormComponent = (function () {
    function InscriptionFormComponent(fb) {
        this.fb = fb;
        this.submitted = false;
        this.user = new user_1.User(11, '', 'Salah', 'benamor.salah1991@gmail.com');
        this.formErrors = {
            'nom': '',
            'prenom': '',
            'email': '',
            'password': ''
        };
        this.validationMessages = {
            'nom': {
                'required': 'Ce champs est obligatoire'
            },
            'prenom': {
                'required': 'Ce champs est obligatoire'
            },
            'email': {
                'forbiddenName': 'Valeur invalide',
                'required': 'Ce champs est obligatoire'
            },
            'password': {
                'required': 'Ce champs est obligatoire'
            }
        };
    }
    InscriptionFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    InscriptionFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.loginForm = this.fb.group({
            nom: ["", forms_1.Validators.required],
            prenom: ["",],
            email: ["", [forms_1.Validators.required,
                    formValidator_1.forbiddenNameValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            password: ["", forms_1.Validators.required]
        });
        this.loginForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    InscriptionFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
    };
    InscriptionFormComponent.prototype.onValueChanged = function (data) {
        console.log("value change");
        if (!this.loginForm) {
            return;
        }
        var form = this.loginForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };
    return InscriptionFormComponent;
}());
InscriptionFormComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: './inscription-form.html',
        styles: ["\n    .ng-valid { border-color: green; }\n    .ng-invalid { border-color: red; }    \n  "]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder])
], InscriptionFormComponent);
exports.InscriptionFormComponent = InscriptionFormComponent;
//# sourceMappingURL=inscription.component.js.map