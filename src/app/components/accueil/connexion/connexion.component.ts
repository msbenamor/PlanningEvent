import { Component, OnInit }  from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User }       from './../../../Entity/user';
import { forbiddenNameValidator } from '../../../common/formValidator';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'connexion-form',
  templateUrl: './connexion-form.html',
  styles: [`
    .ng-valid { border-color: green; }
    .ng-invalid { border-color: red; }    
  `]
})
export class ConnexionFormComponent implements OnInit{
  connexionForm: FormGroup;
  private loggedIn = false;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private route: Router,
    ) { 
      this.loggedIn = !!localStorage.getItem('auth_token');
    }
    ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
  this.connexionForm = this.fb.group({
    email: ["", [Validators.required,
              forbiddenNameValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    password: ["", Validators.required]
  });

    this.connexionForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); 
  }
  submitted = false;
  onSubmit() 
  {
    if(this.connexionForm.valid){
      this.userService.login(this.user.mail, this.user.password);
      this.submitted = true;
      localStorage.setItem('auth_token', 'ici_mettre_tocken');
      this.loggedIn = true;
    }
  }
  user = new User();
  onValueChanged(data?: any) {
    if (!this.connexionForm) { return; }
    const form = this.connexionForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  formErrors = {
    'email' : '',
    'password' : ''
  };

  validationMessages = {
    'email': {
      'forbiddenName': 'Valeur invalide',
      'required':      'Ce champs est obligatoire'
      
    },
    'password': {
      'required':      'Ce champs est obligatoire'
    }
  };
}