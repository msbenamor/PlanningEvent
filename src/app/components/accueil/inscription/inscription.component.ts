import { Component, OnInit }  from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User }       from './../../../Entity/user';
import { forbiddenNameValidator } from '../../../common/formValidator';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
@Component({
  selector: 'inscription-form',
  templateUrl: './inscription-form.html',
  styles: [`
    .ng-valid { border-color: green; }
    .ng-invalid { border-color: red; }    
  `]
})
export class InscriptionFormComponent implements OnInit{
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private route: Router,
    ) { }
    ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
  this.loginForm = this.fb.group({
    nom: ["", Validators.required],
    prenom: ["",Validators.required],
    mail: ["", [Validators.required,
              forbiddenNameValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    password: ["", Validators.required],
    dateNaissance: ["", Validators.required],
  });

    this.loginForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  submitted = false;
  onSubmit() 
  { 
    this.submitted = true; 
    this.userService.create(this.user);
  }
  user = new User();
  onValueChanged(data?: any) {
    console.log("value change");
    if (!this.loginForm) { return; }
    const form = this.loginForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }
  formErrors = {
    'nom': '',
    'prenom' : '',
    'mail' : '',
    'password' : '',
    'dateNaissance': '',
  };

  validationMessages = {
    'nom': {
      'required':      'Ce champs est obligatoire'
    },
    'prenom': {
      'required':      'Ce champs est obligatoire'
    },
    'mail': {
      'forbiddenName': 'Valeur invalide',
      'required':      'Ce champs est obligatoire'
      
    },
    'password': {
      'required':      'Ce champs est obligatoire'
    },
    'dateNaissance': {
      'required':      'Ce champs est obligatoire'
    }
  };
}