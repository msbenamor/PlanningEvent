import { Component, OnInit }  from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User }       from './../../../Entity/user';
import { forbiddenNameValidator } from '../../../common/formValidator';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
@Component({
  selector: 'liste-users',
  templateUrl: './liste-users.html'
})
export class ListeUsersComponent implements OnInit{
    constructor(
    private userService: UserService,
    private route: Router,
    ) { }
  users: User[];

  getUsers(): void {
    this.userService
        .getUsers()
        .then(users => this.users = users);
  }
  
  ngOnInit(): void {
    this.getUsers();
  }
}