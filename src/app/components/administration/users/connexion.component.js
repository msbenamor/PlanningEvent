"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var user_1 = require("./../../../Entity/user");
var formValidator_1 = require("../../../common/formValidator");
var router_1 = require("@angular/router");
var user_service_1 = require("./../../../services/user.service");
var ConnexionFormComponent = (function () {
    function ConnexionFormComponent(fb, userService, route) {
        this.fb = fb;
        this.userService = userService;
        this.route = route;
        this.submitted = false;
        this.user = new user_1.User();
        this.formErrors = {
            'email': '',
            'password': ''
        };
        this.validationMessages = {
            'email': {
                'forbiddenName': 'Valeur invalide',
                'required': 'Ce champs est obligatoire'
            },
            'password': {
                'required': 'Ce champs est obligatoire'
            }
        };
    }
    ConnexionFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    ConnexionFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.connexionForm = this.fb.group({
            email: ["", [forms_1.Validators.required,
                    formValidator_1.forbiddenNameValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            password: ["", forms_1.Validators.required]
        });
        this.connexionForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged(); // (re)set validation messages now
    };
    ConnexionFormComponent.prototype.onSubmit = function () {
        if (this.connexionForm.valid) {
            console.log("le formulaire est valide");
            this.submitted = true;
        }
    };
    ConnexionFormComponent.prototype.onValueChanged = function (data) {
        if (!this.connexionForm) {
            return;
        }
        var form = this.connexionForm;
        for (var field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };
    return ConnexionFormComponent;
}());
ConnexionFormComponent = __decorate([
    core_1.Component({
        selector: 'connexion-form',
        templateUrl: './connexion-form.html',
        styles: ["\n    .ng-valid { border-color: green; }\n    .ng-invalid { border-color: red; }    \n  "]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        user_service_1.UserService,
        router_1.Router])
], ConnexionFormComponent);
exports.ConnexionFormComponent = ConnexionFormComponent;
//# sourceMappingURL=connexion.component.js.map