import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Event }                from './../../../Entity/event';
import { EventService }         from './../../../services/event.service';

@Component({
  selector: 'my-events',
  templateUrl: './events.component.html',
  styleUrls: [ './events.component.css' ]
})

export class EventsComponent implements OnInit {
  events: Event[];
  selectedEvent: Event;

  constructor(
    private eventService: EventService,
    private router: Router) { }

  getEvents(): void {
    this.eventService
        .getEvents()
        .then(events => this.events = events);
  }

  delete(event: Event): void {
    this.eventService
        .delete(event.id)
        .then(() => {
          this.events = this.events.filter(h => h !== event);
          if (this.selectedEvent === event) { this.selectedEvent = null; }
        });
  }

  ngOnInit(): void {
    this.getEvents();
  }

  onSelect(event: Event): void {
    this.selectedEvent = event;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedEvent.id]);
  }
}