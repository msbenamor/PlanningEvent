"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var formValidator_1 = require("../../../common/formValidator");
var event_1 = require("../../../Entity/event");
var event_service_1 = require("./../../../services/event.service");
var CreateEventComponent = (function () {
    function CreateEventComponent(fb, eventService, router) {
        this.fb = fb;
        this.eventService = eventService;
        this.router = router;
        this.event = new event_1.Event();
        this.formErrors = {
            'nomEvent': '',
            'description': '',
            'dateDebut': '',
            'dateFin': ''
        };
        this.validationMessages = {
            'nomEvent': {
                'required': 'Ce champs est obligatoire'
            },
            'description': {
                'required': 'Ce champs est obligatoire'
            },
            'dateDebut': {
                'dateDebutFin': 'La date de début doit être antérieure à la date de fin'
            },
            'dateFin': {
                'dateDebutFin': 'La date de fin doit être postérieure à la date de début'
            },
        };
    }
    CreateEventComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    CreateEventComponent.prototype.buildForm = function () {
        var _this = this;
        this.evenementForm = this.fb.group({
            nomEvent: ["", forms_1.Validators.required],
            description: ["", forms_1.Validators.required],
            dateDebut: ["", [forms_1.Validators.required, formValidator_1.dateDebutFinValidator(this.event)]],
            dateFin: ["", [forms_1.Validators.required, formValidator_1.dateDebutFinValidator(this.event)]]
        });
        this.evenementForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
    };
    CreateEventComponent.prototype.addEvent = function () {
        this.eventService.create(this.event)
            .then(function (event) {
            console.log('event created');
        });
    };
    CreateEventComponent.prototype.onValueChanged = function (data) {
        if (!this.evenementForm) {
            return;
        }
        var form = this.evenementForm;
        for (var field in this.formErrors) {
            this.formErrors[field] = '';
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    console.log(key);
                    this.formErrors[field] += messages[key] + ' ';
                    if (field == 'dateDebut') {
                        this.formErrors['dateFin'] += messages[key] + ' ';
                    }
                    if (field == 'dateFin') {
                        this.formErrors['dateDebut'] += messages[key] + ' ';
                    }
                }
            }
        }
    };
    CreateEventComponent.prototype.onSubmit = function () {
        this.addEvent();
    };
    return CreateEventComponent;
}());
CreateEventComponent = __decorate([
    core_1.Component({
        selector: 'create-event',
        templateUrl: './create-event.component.html'
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        event_service_1.EventService,
        router_1.Router])
], CreateEventComponent);
exports.CreateEventComponent = CreateEventComponent;
//# sourceMappingURL=create-event.component.js.map