import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { dateDebutFinValidator } from '../../../common/formValidator';
import { Event }                from '../../../Entity/event';
import { EventService }         from './../../../services/event.service';

@Component({
  selector: 'create-event',
  templateUrl: './create-event.component.html'
})
export class CreateEventComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private eventService: EventService,
    private router: Router) { }

  ngOnInit(): void {
    this.buildForm();
  }

  evenementForm: FormGroup;
  event = new Event();
  createdEvent : Event;
  buildForm(): void {
  this.evenementForm = this.fb.group({
    nomEvent: ["", Validators.required],
    description: ["", Validators.required],
    dateDebut : ["", [Validators.required,dateDebutFinValidator(this.event)]],
    dateFin : ["", [Validators.required,dateDebutFinValidator(this.event)]]
  });

  this.evenementForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  addEvent(): void {
    this.eventService
    .create(this.event)
    .then(event => this.router.navigate(['event/' + event.id]));
  }

  onValueChanged(data?: any) {
    if (!this.evenementForm) { return; }
    const form = this.evenementForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
          if(field == 'dateDebut'){
            this.formErrors['dateFin'] += messages[key] + ' ';
          }
          if(field == 'dateFin'){
            this.formErrors['dateDebut'] += messages[key] + ' ';
          }
        }
      }
    }
  }
  onSubmit() 
  { 
    this.addEvent();
  }
    formErrors = {
    'nomEvent' : '',
    'description' : '',
    'dateDebut' : '',
    'dateFin' : ''
  };

  validationMessages = {
    'nomEvent': {
      'required':      'Ce champs est obligatoire'
    },
    'description': {
      'required':      'Ce champs est obligatoire'
    },
    'dateDebut': {
      'dateDebutFin':      'La date de début doit être antérieure à la date de fin'
    },
    'dateFin': {
      'dateDebutFin':      'La date de fin doit être postérieure à la date de début'
    },
  };
}