import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }            from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { dateDebutFinValidator } from '../../../common/formValidator';
import { Event }                from '../../../Entity/event';
import { EventService }         from './../../../services/event.service';
import { Location }               from '@angular/common';
@Component({
  selector: 'create-event',
  templateUrl: './event.component.html'
})
export class EventComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private eventService: EventService,
    private router: Router,
    private route: ActivatedRoute,
    ) { }

  evenementForm: FormGroup;
  event = new Event();
  ngOnInit(): void {
        this.route.params
      .switchMap((params: Params) => this.eventService.getEvent(params['id']))
      .subscribe(event => this.event = event);
  }
}