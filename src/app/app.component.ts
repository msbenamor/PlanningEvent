import { Component }          from '@angular/core';
import { Router } from '@angular/router';
import { LoggedInGuard } from './common/Guardin';
import { UserService } from './services/user.service';
@Component({
  selector: 'app-root',
  template: `
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li  class="active"><a href="#">Home</a></li>
      <li *ngIf = "loggedInGuard.canActivate()"><a routerLink="/dashboard" routerLinkActive="active">Dashboard</a></li>
      <li *ngIf = "loggedInGuard.canActivate()"><a routerLink="/events" routerLinkActive="active">Events</a></li>
      <li *ngIf = "loggedInGuard.canActivate()"><a routerLink="/utilisateurs" routerLinkActive="active">Utilisateurs</a></li>
      <li *ngIf = "loggedInGuard.canActivate()"><a routerLink="/create" routerLinkActive="active">Create Event</a></li>
      <li *ngIf = "!loggedInGuard.canActivate()"><a routerLink="/inscription" routerLinkActive="active">Inscription</a></li>
      <li *ngIf = "!loggedInGuard.canActivate()"><a routerLink="/connexion" routerLinkActive="active">Connexion</a></li>
      <li *ngIf = "loggedInGuard.canActivate()"><a routerLink="#" (click)="userService.logout()" routerLinkActive="active">Déconnexion</a></li>
    </ul>
  </div>
</nav>
<router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css'],
  providers: [ LoggedInGuard ],
})
export class AppComponent {
    constructor(
    private route: Router,
    private loggedInGuard: LoggedInGuard,
    private userService: UserService,
    ) { }
  title = 'My Events';
}