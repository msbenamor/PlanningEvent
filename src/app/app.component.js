"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'My Events';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: "\n<nav class=\"navbar navbar-default\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <a class=\"navbar-brand\" href=\"#\">WebSiteName</a>\n    </div>\n    <ul class=\"nav navbar-nav\">\n      <li class=\"active\"><a href=\"#\">Home</a></li>\n      <li><a routerLink=\"/dashboard\" routerLinkActive=\"active\">Dashboard</a></li>\n      <li><a routerLink=\"/events\" routerLinkActive=\"active\">Events</a></li>\n      <li><a routerLink=\"/inscription\" routerLinkActive=\"active\">Inscription</a></li>\n      <li><a routerLink=\"/connexion\" routerLinkActive=\"active\">Connexion</a></li>\n      <li><a routerLink=\"/utilisateurs\" routerLinkActive=\"active\">Utilisateurs</a></li>\n      <li><a routerLink=\"/create\" routerLinkActive=\"active\">Create Event</a></li>\n    </ul>\n  </div>\n</nav>\n<router-outlet></router-outlet>\n  ",
        styleUrls: ['./app.component.css']
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map