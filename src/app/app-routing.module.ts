import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './components/dashBord/dashboard.component';
import { EventsComponent }      from './components/events/myEvents/events.component';
import { EventComponent }      from './components/events/event/event.component';
import { InscriptionFormComponent }  from './components/accueil/inscription/inscription.component';
import { ConnexionFormComponent }  from './components/accueil/connexion/connexion.component';
import { ListeUsersComponent }  from './components/administration/users/liste-users.component';
import { CreateEventComponent }  from './components/events/create/create-event.component';
import { LoggedInGuard } from './common/Guardin';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent , canActivate: [LoggedInGuard]},
  { path: 'event/:id', component: EventComponent },
  { path: 'events',     component: EventsComponent , canActivate: [LoggedInGuard]},
  { path: 'utilisateurs',     component: ListeUsersComponent , canActivate: [LoggedInGuard]},
  { path: 'create',     component: CreateEventComponent , canActivate: [LoggedInGuard]},
    { path: 'inscription',     component: InscriptionFormComponent },
  { path: 'connexion',     component: ConnexionFormComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}