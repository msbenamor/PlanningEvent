import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
//import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
//import { InMemoryDataService }  from './inMemoryData/in-memory-events.service';
import { AppComponent }         from './app.component';
import { DashboardComponent }   from './components/dashBord/dashboard.component';
import { EventsComponent }      from './components/events/myEvents/events.component';
import { EventService }          from './services/event.service';
import { UserService }          from './services/user.service';
import { InscriptionFormComponent }  from './components/accueil/inscription/inscription.component';
import { ConnexionFormComponent }  from './components/accueil/connexion/connexion.component';
import { ListeUsersComponent }  from './components/administration/users/liste-users.component';
import { CreateEventComponent }  from './components/events/create/create-event.component';
import { EventComponent } from './components/events/event/event.component';
import { LoggedInGuard } from './common/Guardin';
import { environment } from './../environments/environment';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //!environment.production ? InMemoryWebApiModule.forRoot(InMemoryDataService) : [],
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    EventsComponent,
    InscriptionFormComponent,
    ConnexionFormComponent,
    ListeUsersComponent,
    CreateEventComponent,
    EventComponent,
  ],
  providers: [ EventService, UserService, LoggedInGuard ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
