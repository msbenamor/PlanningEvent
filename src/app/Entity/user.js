"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = (function () {
    function User(obj) {
        this.id = obj && obj.id || 0;
        this.nom = obj && obj.nom || '';
        this.prenom = obj && obj.prenom || '';
        this.mail = obj && obj.mail || '';
        this.password = obj && obj.password || '';
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.js.map