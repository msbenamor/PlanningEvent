"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Event = (function () {
    function Event(obj) {
        this.id = obj && obj.id || 0;
        this.nom = obj && obj.nom || '';
        this.description = obj && obj.description || '';
        this.dateDebut = obj && obj.dateDebut || null;
        this.dateFin = obj && obj.dateFin || null;
    }
    return Event;
}());
exports.Event = Event;
//# sourceMappingURL=event.js.map