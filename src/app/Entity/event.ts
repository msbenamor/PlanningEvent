import { User } from './user';

interface IEvent{
  organisateur? : User;
  id? : number;
  nomEvent? : string;
  description?: string;
  dateDebut?: string;
  dateFin? : string;
}
export class Event {
  organisateur? : User;
  public id? : number;
  public nomEvent? : string;
  public description? : string;
  public dateDebut? : string;
  public dateFin? : string;

  constructor(obj?: IEvent) {
    this.organisateur = obj && obj.organisateur || 0
    this.id = obj && obj.id || 0
    this.nomEvent = obj && obj.nomEvent || ''
    this.description = obj && obj.description || ''
    this.dateDebut = obj && obj.dateDebut || null;
    this.dateFin = obj && obj.dateFin || null;
  }
}