interface IPerson{
   id : number;
   nom: string;
   prenom: string;
   mail: string;
   password: string;
   date_naissance: string;
}
export class User {
    public id? : number;
    public nom? : string;
    public prenom? : string;
    public mail? : string;
    public password? : string;
    public date_naissance?: string;

  constructor(obj?: IPerson) {
    this.id = obj && obj.id || 0
    this.nom = obj && obj.nom || ''
    this.prenom = obj && obj.prenom || ''
    this.mail = obj && obj.mail || '';
    this.password = obj && obj.password || '';
    this.date_naissance = obj && obj.date_naissance || '';
  }
}