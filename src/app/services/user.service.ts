import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';
import { User } from './../Entity/user';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  private headers = null;
  private userUrl = 'http://ec2-54-191-81-87.us-west-2.compute.amazonaws.com:1337/user';
  private userByMail = 'api/userByMail';
  private loggedIn = false;

  constructor(
    private http: Http,
    private route:Router,
    ) {
    this.loggedIn = !!localStorage.getItem('auth_token');
    this.headers = new Headers({'Content-Type': 'application/json'});
    this.headers.append('Accept', 'application/json');
   }

  isLoggedIn() {
    return this.loggedIn;
  }

  logout() {
    localStorage.removeItem('auth_token');
    this.loggedIn = false;
    this.route.navigate(['connexion']);
  }

  login(email, password) {
    localStorage.setItem('auth_token', 'ici_mettre_tocken');
    this.loggedIn = true;
    console.log("user is logged");
    this.route.navigate(['events']);
    return true;
  }

  getUsers(): Promise<User[]> {
    return this.http.get(this.userUrl)
               .toPromise()
               .then(users => users.json() as User[])
               .catch(this.handleError);
  }

  getUsersByMail(mail : string): Promise<User[]> {
    return this.http.get(this.userByMail)
               .toPromise()
               .then(response => response.json().data as User)
               .catch(this.handleError);
  }

  getUser(id: number): Promise<User> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as User)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.userUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  isMailValid(mail : string){
    this.http.get("https://apilayer.net/api/check?access_key=a18cc513afae3ca88b15f194a21db62f&email=benamor.salah1991@gmail.com")
    .map(response => response.json().data).subscribe(data => function(data){
      console.log("success");
      alert(data);
    }, err => function(err){
      console.log("err");
      alert("erreur "+err);
    })
  }

  create(user: User): void {
     //this.isMailValid(user.mail);
     this.http
      .post(this.userUrl, 
      JSON.stringify({
                    nom: user.nom,
                    prenom : user.prenom,
                    mail : user.mail,
                    date_naissance: user.date_naissance, 
                    password : user.password,
                    }),
      {headers: this.headers})
      .map(response => response.json()).subscribe(res => function(){
        alert("test");
      })
  }

  update(user: User): Promise<User> {
    const url = `${this.userUrl}/${user.id}`;
    return this.http
      .put(url, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(() => user)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}