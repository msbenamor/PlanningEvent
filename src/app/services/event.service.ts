
import { Event } from './../Entity/event';
import 'rxjs/add/operator/toPromise';
import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';
import { User } from './../Entity/user';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class EventService {
  connected : Boolean = (localStorage.getItem('user') != null);
  private headers = new Headers({'Content-Type': 'application/json'});
  private eventsUrl = 'http://ec2-54-191-81-87.us-west-2.compute.amazonaws.com:1337/event';  // URL to web api

  constructor(private http: Http) { }

  getEvents(): Promise<Event[]> {
    return this.http.get(this.eventsUrl)
               .toPromise()
               .then(response => response.json() as Event[])
               .catch(this.handleError);
  }
  
  getEvent(id: number): Promise<Event> {
    const url = `${this.eventsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Event)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.eventsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(event: Event) : Promise<Event> {
      return this.http
      .post(this.eventsUrl, 
      JSON.stringify({
                      nom: event.nomEvent,
                      description : event.description,
                      dateDebut : event.dateDebut,
                      dateFin : event.dateFin,
                      organisateur : 2,
                    }),
      {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Event)
      .catch(this.handleError);
  }

  update(event: Event): Promise<Event> {
    const url = `${this.eventsUrl}/${event.id}`;
    return this.http
      .put(url, JSON.stringify(event), {headers: this.headers})
      .toPromise()
      .then(() => event)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

