"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var events = [{ nom: 'event1', description: 'balade', dateDebut: '16/03/1991', dateFin: '04/02/1995' }];
        var users = [
            { id: 11, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 12, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 13, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 14, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 15, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 16, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 18, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' },
            { id: 19, nom: 'BENAMOR', prenom: 'salah', mail: 'me@me.fr', password: 'me123' }
        ];
        return { events: events, users: users };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-events.service.js.map